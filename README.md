# 카카오페이 뿌리기 기능 구현

## 요구사항
* 뿌리기, 받기, 조회 기능을 수행하는 REST API 를 구현합니다.
    * 요청한 사용자의 식별값은 숫자 형태이며 "X-USER-ID" 라는 HTTP Header로 전달됩니다.
    * 요청한 사용자가 속한 대화방의 식별값은 문자 형태이며 "X-ROOM-ID" 라는 HTTP Header로 전달됩니다.
    * 모든 사용자는 뿌리기에 충분한 잔액을 보유하고 있다고 가정하여 별도로 잔액에 관련된 체크는 하지 않습니다.
* 작성하신 어플리케이션이 다수의 서버에 다수의 인스턴스로 동작하더라도 기능에 문제가 없도록 설계되어야 합니다.
* 각 기능 및 제약사항에 대한 단위테스트를 반드시 작성합니다.

## 개발환경
- Open_JDK 1.8.0_275
- Spring Boot 2.4.3
- JPA 2.4.3
- MariaDB 10.4.17
- Spring Web Flux 2.4.3

## API 상세 구현
### 뿌리기 API
* Reqest
    ```
    curl --location --request POST 'localhost:8080/api/v1/share' \
    --header 'X-USER-ID: 2' \
    --header 'X-ROOM-ID: 10' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "countM": 3000,
        "countP": 3
    }'
    ```
* Response
    ```json
    {
        "status": "Success",
        "statusCode": 200,
        "data": {
            "token": "hry"
        }
    }
    ```
### 받기 API
* Request
    ```
    curl --location --request PUT 'localhost:8080/api/v1/share' \
    --header 'X-USER-ID: 3' \
    --header 'X-ROOM-ID: 10' \
    --header 'X-TOKEN-ID: hry'
    ```
* Response
  ```json
  {
    "status": "Success",
    "statusCode": 200,
    "data": {
        "countM": 2982
    }
  }
  ```
### 조회 API
* Request
    ```
    curl --location --request GET 'localhost:8080/api/v1/share' \
    --header 'X-USER-ID: 2' \
    --header 'X-TOKEN-ID: hry'
    ```
* Response
  ```json
  {
    "status": "Success",
    "statusCode": 200,
    "data": {
        "regDate": "2021-03-01T16:06:05.69",
        "countM": 3000,
        "receiveM": 2982,
        "receiveMList": [
            {
                "userId": 3,
                "receiveM": 2982
            }
        ]
    }
  }
  ```

## 상세구현 내용

### 뿌리기 API
* token은 3자리 문자열로 구성되며 예측이 불가능해야 합니다.
    * 영문 3자리 랜덤 함수 구현
    * 경우의 수 = 17576 (a~z,a~z,a~z)
    * 코어 로직 전 Table TotalCnt 와 경우의 수 비교

### 받기 API
* 뿌린 건은 10분간만 유효합니다. 뿌린지 10분이 지난 요청에 대해서는 받기
  실패 응답이 내려가야 합니다.
    ```
    String X_TOKEN = "cbt";
    Optional<Share> share = shareRepo.findShareByToken(X_TOKEN);
    share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
    Share shareT = share.get();

    // 10분 이내
    LocalDateTime nowTime = LocalDateTime.now();
    LocalDateTime plusMinutes = shareT.getRegDate().plusMinutes(10);
    if(plusMinutes.isBefore(nowTime)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_001);
    ```
* 뿌린기가 호출된 대화방과 동일한 대화방에 속한 사용자만이 받을 수
  있습니다.
    ```
      String X_TOKEN = "cbt";
      Long X_ROOM = 10l;
  
      Optional<Share> share = shareRepo.findShareByToken(X_TOKEN);
      share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
      Share shareT = share.get();
      // 동일한 방 체크
      if(!shareT.getRoomId().equals(X_ROOM)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_002);
    ```
* 자신이 뿌리기한 건은 자신이 받을 수 없습니다.
    ```
      String X_TOKEN = "cbt";
      Long X_USER = 1l;
  
      Optional<Share> share = shareRepo.findShareByToken(X_TOKEN);
      share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
      Share shareT = share.get();
      // 뿌리기한 사람이 요청시
      if(shareT.getMasterId().equals(X_USER)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_003);
    ```
* 뿌리기당한사용자는한번만받을수있습니다.
    ```
      String X_TOKEN = "cbt";
      Long X_USER = 1l;
  
      Optional<Share> share = shareRepo.findShareByToken(X_TOKEN);
      share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
      Share shareT = share.get();
  
      // 뿌리기 당 한번만 요청 가능
      Optional<ShareItem> shareItemT = Optional.empty();
      List<ShareItem> shareItems = shareT.getShareItems();
      for(ShareItem shareItem:shareItems){
          if(shareItem.getUserId() == X_USER){
              throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_004);
          }else if(shareItem.getReceiveYn().equals(false)){
              shareItemT = Optional.of(shareItem);
          }
      }
    ```
* 모두 지급 받은 경우
    ```
      String X_TOKEN = "cbt";
      Long X_USER = 1l;
    
      Optional<Share> share = shareRepo.findShareByToken(X_TOKEN);
      share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
      Share shareT = share.get();
    
      // 뿌리기 당 한번만 요청 가능
      Optional<ShareItem> shareItemT = Optional.empty();
      List<ShareItem> shareItems = shareT.getShareItems();
      for(ShareItem shareItem:shareItems){
          if(shareItem.getUserId() == X_USER){
              throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_004);
          }else if(shareItem.getReceiveYn().equals(false)){
              shareItemT = Optional.of(shareItem);
          }
      }
      // 선착순 마감
      shareItemT.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_005));
    ```

### 조회 API
* 뿌린 사람 자신만 조회를 할 수 있습니다. 다른사람의 뿌리기건이나 유효하지
  않은 token에 대해서는 조회 실패 응답이 내려가야 합니다.
    ```
    Optional<Share> share = shareRepo.findShareByToken(token);
    share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
    Share shareT = share.get();

    // 뿌린 사람만 조회 가능
    if(!shareT.getMasterId().equals(userId)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_006);
    ```
* 뿌린건에대한조회는7일동안할수있습니다.
    ```
  // 7일 이내 조회요청 확인
  LocalDateTime nowTime = LocalDateTime.now();
  LocalDateTime plusDays = shareT.getRegDate().plusDays(7);
  if(plusDays.isBefore(nowTime)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_007);
    ```
## 핵심 문제해결 전략
* 어플레케이션은 다량의 트래픽에도 무리가 없도록 효율적으로 작성되었는가?
  * 기본 Spring MVC 패턴으로 개발을 완료하였으나 쓰레드 블럭을 막기위해 Web Flux 별도 개발(별도 브랜치)
  * 어노테이 기반 라우팅 과 함수 기반 라우팅이 존재하였지만 기존 로직의 재사용성을 고려하여 어노테이션 기반으로 개발
  * 본 프로젝트에서는 R2DBC 는 제외하였습니다.
