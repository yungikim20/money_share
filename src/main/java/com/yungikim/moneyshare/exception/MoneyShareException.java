package com.yungikim.moneyshare.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
public class MoneyShareException extends RuntimeException{
    private HttpStatus httpStatus;
    private String message;
    private LocalDateTime exceptionTime;

    public MoneyShareException(HttpStatus httpStatus,String message){
        this.httpStatus = httpStatus;
        this.message = message;
        this.exceptionTime = LocalDateTime.now();
    }
}
