package com.yungikim.moneyshare.exception;

import com.yungikim.moneyshare.common.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class MoneyShareAdvice {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(value = MoneyShareException.class)
    public ResponseEntity moneyShareException(MoneyShareException ex){
        logger.error("moneyShareException: " + ex.getMessage());
        return new ResponseEntity(new Response(ex),ex.getHttpStatus());
    }
}
