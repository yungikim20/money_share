package com.yungikim.moneyshare.common.response;

public class ResponseMsg {
    public static final String RESPONSE_MSG_BAD_000 = "잘못된 요청입니다.";
    public static final String RESPONSE_MSG_BAD_001 = "뿌린 건은 10분간만 유효합니다.";
    public static final String RESPONSE_MSG_BAD_002 = "동일한 대화방에 속한 사용자만 받을 수 있습니다.";
    public static final String RESPONSE_MSG_BAD_003 = "자신이 뿌리기한 건은 자신이 받을 수 없습니다.";
    public static final String RESPONSE_MSG_BAD_004 = "뿌리기 당 한 사용자는 한번만 받을 수 있습니다.";
    public static final String RESPONSE_MSG_BAD_005 = "석착순 마감 되었습니다.";
    public static final String RESPONSE_MSG_BAD_006 = "뿌린 사람 자신만 조회를 할 수 있습니다.";
    public static final String RESPONSE_MSG_BAD_007 = "뿌린 건에 대한 조회는 7일 동안 할 수 있습니다.";
    public static final String RESPONSE_MSG_SERVER_ERROR = "내부 시스템 오류";
}
