package com.yungikim.moneyshare.common.response;

import com.yungikim.moneyshare.exception.MoneyShareException;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
public class Response {
    private HttpStatus httpStatus;
    private String message;
    private LocalDateTime exceptionTime;

    public Response(MoneyShareException ex) {
        this.httpStatus = ex.getHttpStatus();
        this.message = ex.getMessage();
        this.exceptionTime = ex.getExceptionTime();
    }
}
