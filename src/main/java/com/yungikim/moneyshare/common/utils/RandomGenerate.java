package com.yungikim.moneyshare.common.utils;

import com.yungikim.moneyshare.model.dto.ShareDTO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.Random;

@Configuration
public class RandomGenerate {
    public String generatedString(int targetStringLength){
        /*
        난수 설정
        https://linkeverything.github.io/java/java-random-string/
         */
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();
        String generatedString = random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return generatedString;
    }
    public long[] randomSplit(ShareDTO.Request request){
        long[] nums = new long[Long.valueOf(request.getCountP()).intValue()];
        long total = request.getCountM();
        Random rand = new Random();
        for (int i = 0; i < nums.length-1; i++) {
            nums[i] = rand.nextInt(Long.valueOf(total).intValue());
            total -= nums[i];
        }
        nums[nums.length-1] = total;
        Arrays.sort(nums);
        return nums;
    }
}
