package com.yungikim.moneyshare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@SpringBootApplication
public class MoneyshareApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoneyshareApplication.class, args);
    }

}
