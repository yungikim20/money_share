package com.yungikim.moneyshare.service.Impl;

import com.yungikim.moneyshare.common.response.ResponseMsg;
import com.yungikim.moneyshare.common.utils.RandomGenerate;
import com.yungikim.moneyshare.exception.MoneyShareException;
import com.yungikim.moneyshare.model.domain.Share;
import com.yungikim.moneyshare.model.domain.ShareItem;
import com.yungikim.moneyshare.model.dto.ShareDTO;
import com.yungikim.moneyshare.persistence.ShareItemRepository;
import com.yungikim.moneyshare.persistence.ShareRepository;
import com.yungikim.moneyshare.service.ShareService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ShareServiceImpl implements ShareService {

    private final RandomGenerate randomGenerate;
    private final ShareRepository shareRepo;
    private final ShareItemRepository shareItemRepo;
    private final Long TOKEN_OF_CASES = 17576l;

    @Override
    public ShareDTO.Share share(Long masterId, String roomId, ShareDTO.Request request) {
        String token = randomGenerate.generatedString(3);

        // while 문에서 무한루프가 걸릴 수 있으므로 총 갯수 검사(token 경우의 수 17576)
        long totalCnt = shareRepo.count();
        if(totalCnt > TOKEN_OF_CASES) throw new MoneyShareException(HttpStatus.INTERNAL_SERVER_ERROR,ResponseMsg.RESPONSE_MSG_SERVER_ERROR);

        while (shareRepo.existsByToken(token)){
            token = randomGenerate.generatedString(3);
        }

        Share share = new Share(masterId,
                request.getCountM(),
                request.getCountP(),
                token,
                roomId);
        Share saveShare = shareRepo.save(share);

        List<ShareItem> shareItems = new ArrayList<>();
        long[] splitMoney = randomGenerate.randomSplit(request);
        for(long split : splitMoney){
            ShareItem shareItem = new ShareItem(saveShare,split,false);
            shareItems.add(shareItem);
        }
        shareItemRepo.saveAll(shareItems);
        return new ShareDTO.Share(token);
    }

    @Override
    @Transactional
    public ShareDTO.Receive receive(Long userId, String roomId, String token) {
        Optional<Share> share = shareRepo.findShareByToken(token);
        share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
        Share shareT = share.get();

        // 10분 이내
        LocalDateTime nowTime = LocalDateTime.now();
        LocalDateTime plusMinutes = shareT.getRegDate().plusMinutes(10);
        if(plusMinutes.isBefore(nowTime)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_001);

        // 동일한 방 체크
        if(!shareT.getRoomId().equals(roomId)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_002);

        // 뿌리기한 사람이 요청시
        if(shareT.getMasterId().equals(userId)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_003);

        // 뿌리기 당 한번만 요청 가능
        Optional<ShareItem> shareItemT = Optional.empty();
        List<ShareItem> shareItems = shareT.getShareItems();
        for(ShareItem shareItem:shareItems){
            if(shareItem.getUserId() == userId){
                throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_004);
            }else if(shareItem.getReceiveYn().equals(false)){
                shareItemT = Optional.of(shareItem);
            }
        }

        // 선착순 마감
        shareItemT.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_005));

        // 지급처리
        ShareItem saveShareItem = shareItemT.get();
        long countM = saveShareItem.receive(userId);
        return new ShareDTO.Receive(countM);
    }

    @Override
    @Transactional
    public ShareDTO.Detail detail(Long userId, String token) {
        Optional<Share> share = shareRepo.findShareByToken(token);
        share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
        Share shareT = share.get();

        // 뿌린 사람만 조회 가능
        if(!shareT.getMasterId().equals(userId)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_006);

        // 7일 이내 조회요청 확인
        LocalDateTime nowTime = LocalDateTime.now();
        LocalDateTime plusDays = shareT.getRegDate().plusDays(7);
        if(plusDays.isBefore(nowTime)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_007);

        // 최종 조회
        List<ShareDTO.ReceiveMItem> receiveMItems = new ArrayList<>();
        long receiveM = 0;
        List<ShareItem> shareItems = shareT.getShareItems();
        for(ShareItem shareItem:shareItems){
            if(shareItem.getReceiveYn().equals(true)){
                receiveM = receiveM + shareItem.getCountM();
                receiveMItems.add(new ShareDTO.ReceiveMItem(shareItem.getUserId(),shareItem.getCountM()));
            }
        }
        ShareDTO.Detail response = new ShareDTO.Detail(shareT.getRegDate(),shareT.getCountM(),receiveM,receiveMItems);
        return response;
    }
}
