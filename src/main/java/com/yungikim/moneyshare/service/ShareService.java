package com.yungikim.moneyshare.service;

import com.yungikim.moneyshare.model.dto.ShareDTO;

public interface ShareService {
    // 뿌리기 Service
    ShareDTO.Share share(Long masterId, String roomId, ShareDTO.Request request);
    // 받기 Service
    ShareDTO.Receive receive(Long userId, String roomId, String token);
    // 조회 Service
    ShareDTO.Detail detail(Long userId, String token);
}
