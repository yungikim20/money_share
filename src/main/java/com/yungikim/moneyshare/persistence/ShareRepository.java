package com.yungikim.moneyshare.persistence;

import com.yungikim.moneyshare.model.domain.Share;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ShareRepository extends JpaRepository<Share,Long> {

    boolean existsByToken(String token);

    long count();

    @Query(value = "select u from Share u where u.token = ?1")
    Optional<Share> findShareByToken(String token);
}
