package com.yungikim.moneyshare.persistence;

import com.yungikim.moneyshare.model.domain.ShareItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ShareItemRepository extends JpaRepository<ShareItem,Long> {
    Optional<ShareItem> findByShare_TokenAndUserId(String x_token, Long x_user);
}
