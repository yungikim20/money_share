package com.yungikim.moneyshare.controller.api.v1;

import com.yungikim.moneyshare.model.dto.ShareDTO;
import com.yungikim.moneyshare.service.ShareService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "${api.version}/share")
@RequiredArgsConstructor
@RestController
public class ShareController {

    private final ShareService shareService;

    @PostMapping
    public ResponseEntity share(
            @RequestHeader("X-USER-ID") Long masterId,
            @RequestHeader("X-ROOM-ID") String roomId,
            @RequestBody ShareDTO.Request request){

        ShareDTO.Response response = new ShareDTO.Response("Success",
                HttpStatus.OK.value(),
                shareService.share(masterId,roomId,request));

        return new ResponseEntity(response, HttpStatus.OK);
    }
    @PutMapping
    public ResponseEntity receive(@RequestHeader("X-USER-ID") Long userId,
                                  @RequestHeader("X-ROOM-ID") String roomId,
                                  @RequestHeader("X-TOKEN-ID") String token){

        ShareDTO.Response response = new ShareDTO.Response("Success",
                HttpStatus.OK.value(),
                shareService.receive(userId,roomId,token));
        return new ResponseEntity(response,HttpStatus.OK);
    }
    @GetMapping
    public ResponseEntity detail(@RequestHeader("X-USER-ID") Long userId,
                                 @RequestHeader("X-TOKEN-ID") String token){

        ShareDTO.Response response = new ShareDTO.Response("Success",
                HttpStatus.OK.value(),
                shareService.detail(userId,token));

        return new ResponseEntity(response,HttpStatus.OK);
    }
}
