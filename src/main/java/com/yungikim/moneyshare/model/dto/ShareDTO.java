package com.yungikim.moneyshare.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

public class ShareDTO {
    @Getter
    public static class Request{
        // 뿌릴금액
        private long countM;
        // 뿌릴인원
        private long countP;

        public Request(long countM, long countP) {
            this.countM = countM;
            this.countP = countP;
        }
    }
    @Getter
    public static class Response{
        private String status;
        private int statusCode;
        private Object data;

        public Response(String status, int statusCode, Object data) {
            this.status = status;
            this.statusCode = statusCode;
            this.data = data;
        }
    }
    @Getter
    public static class Share{
        private String token;

        public Share(String token) {
            this.token = token;
        }
    }
    @Getter
    public static class Receive{
        private Long countM;

        public Receive(Long countM) {
            this.countM = countM;
        }
    }
    @Getter
    public static class Detail{
        private LocalDateTime regDate;
        private Long countM;
        private Long receiveM;
        private List<ShareDTO.ReceiveMItem> receiveMList;

        public Detail(LocalDateTime regDate, Long countM, Long receiveM, List<ShareDTO.ReceiveMItem> receiveMList) {
            this.regDate = regDate;
            this.countM = countM;
            this.receiveM = receiveM;
            this.receiveMList = receiveMList;
        }
    }
    @Getter
    public static class ReceiveMItem{
        private Long userId;
        private Long receiveM;

        public ReceiveMItem(Long userId, Long receiveM) {
            this.userId = userId;
            this.receiveM = receiveM;
        }
    }
}
