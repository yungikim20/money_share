package com.yungikim.moneyshare.model.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Share extends BaseTimeEntity{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long masterId;
    @Column(nullable = false)
    private Long countM;
    @Column(nullable = false)
    private Long countP;

    @Column(nullable = false,length = 3)
    private String token;
    @Column(nullable = false)
    private String roomId;

    @OneToMany(mappedBy = "share")
    private List<ShareItem> shareItems;

    public Share(Long masterId, Long countM, Long countP, String token, String roomId) {
        this.masterId = masterId;
        this.countM = countM;
        this.countP = countP;
        this.token = token;
        this.roomId = roomId;
    }
}
