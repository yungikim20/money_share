package com.yungikim.moneyshare.model.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ShareItem extends BaseTimeEntity{
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "shareId")
    @ManyToOne
    private Share share;

    private Long userId;

    @Column(nullable = false)
    private Long countM;

    @Column(nullable = false)
    private Boolean receiveYn;

    private LocalDateTime receiveDate;

    public ShareItem(Share share, Long countM, Boolean receiveYn) {
        this.share = share;
        this.countM = countM;
        this.receiveYn = receiveYn;
    }
    public long receive(Long userId){
        this.userId = userId;
        this.receiveYn = true;
        this.receiveDate = LocalDateTime.now();
        return countM;
    }
}
