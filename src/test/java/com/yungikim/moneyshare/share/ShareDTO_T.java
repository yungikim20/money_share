package com.yungikim.moneyshare.share;

import com.yungikim.moneyshare.model.dto.ShareDTO;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

public class ShareDTO_T {
    @Getter
    public static class Response{
        private String status;
        private int statusCode;
        private Receive data;

    }
    @Getter
    public static class Receive{
        private Long countM;
    }

    @Getter
    public static class Response_Detail{
        private String status;
        private int statusCode;
        private Detail data;

    }

    @Getter
    public static class Detail{
        private LocalDateTime regDate;
        private Long countM;
        private Long receiveM;
        private List<ShareDTO_T.ReceiveMItem> receiveMList;

        public Detail(LocalDateTime regDate, Long countM, Long receiveM, List<ShareDTO_T.ReceiveMItem> receiveMList) {
            this.regDate = regDate;
            this.countM = countM;
            this.receiveM = receiveM;
            this.receiveMList = receiveMList;
        }
    }
    @Getter
    public static class ReceiveMItem{
        private Long userId;
        private Long receiveM;

        public ReceiveMItem(Long userId, Long receiveM) {
            this.userId = userId;
            this.receiveM = receiveM;
        }
    }
}
