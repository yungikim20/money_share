package com.yungikim.moneyshare.share;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yungikim.moneyshare.common.response.ResponseMsg;
import com.yungikim.moneyshare.exception.MoneyShareException;
import com.yungikim.moneyshare.model.domain.Share;
import com.yungikim.moneyshare.model.domain.ShareItem;
import com.yungikim.moneyshare.model.dto.ShareDTO;
import com.yungikim.moneyshare.persistence.ShareItemRepository;
import com.yungikim.moneyshare.persistence.ShareRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.StopWatch;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class ShareTestController {

    @Autowired
    MockMvc mockMvc;
    @Autowired
    ShareRepository shareRepo;
    @Autowired
    ShareItemRepository shareItemRepo;
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private WebApplicationContext ctx;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx)
                .addFilters(new CharacterEncodingFilter("UTF-8", true))  // 필터 추가
                .alwaysDo(print())
                .build();
    }

    @Test
    public void 뿌리기() throws Exception {
        Long X_USER = 1l;
        Long X_ROOM = 10l;
        String body = objectMapper.writeValueAsString(new ShareDTO.Request(3000,3));

        ResultActions result = mockMvc.perform(post("/api/v1/share")
                .content(body)
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", X_USER)
                .header("X-ROOM-ID", X_ROOM))
                .andExpect(status().isOk())
                .andDo(print());

        String responseBody = result.andReturn().getResponse().getContentAsString();
        ShareDTO.Response response = objectMapper.readValue(responseBody,ShareDTO.Response.class);

        assertNotNull(response.getData());
    }
    @Test
    public void 받기() throws Exception{
        Long X_USER = 2l;
        Long X_ROOM = 10l;
        String X_TOKEN = "cbt";

        ResultActions result = mockMvc.perform(put("/api/v1/share")
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", X_USER)
                .header("X-ROOM-ID", X_ROOM)
                .header("X-TOKEN-ID", X_TOKEN))
                .andExpect(status().isOk())
                .andDo(print());

        String responseBody = result.andReturn().getResponse().getContentAsString();
        ShareDTO_T.Response response = objectMapper.readValue(responseBody,ShareDTO_T.Response.class);

        Optional<ShareItem> shareItem = shareItemRepo.findByShare_TokenAndUserId(X_TOKEN, X_USER);

        assertEquals(response.getData().getCountM(),shareItem.get().getCountM());
    }
    @Test
    public void 조회() throws Exception{
        Long X_USER = 1l;
        String X_TOKEN = "cbt";

        ResultActions result = mockMvc.perform(get("/api/v1/share")
                .contentType(MediaType.APPLICATION_JSON)
                .header("X-USER-ID", X_USER)
                .header("X-TOKEN-ID", X_TOKEN))
                .andExpect(status().isOk())
                .andDo(print());

        String responseBody = result.andReturn().getResponse().getContentAsString();
        ShareDTO_T.Response_Detail response = objectMapper.readValue(responseBody,ShareDTO_T.Response_Detail.class);

        assertNotNull(response.getData());
    }
    @Test
    public void 다량요청() throws Exception{
        String body = objectMapper.writeValueAsString(new ShareDTO.Request(3000,3));

        final StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        for(int i=0;i<1000;i++){
            ResultActions result = mockMvc.perform(post("/api/v1/share")
                    .content(body)
                    .contentType(MediaType.APPLICATION_JSON)
                    .header("X-USER-ID", 1)
                    .header("X-ROOM-ID", 10))
                    .andExpect(status().isOk())
                    .andDo(print());
        }

        stopWatch.stop();
        System.out.println("TotalSeconds = " +stopWatch.getTotalTimeSeconds());
    }
    @Test
    public void 제약사항_10분이내() {
        String X_TOKEN = "cbt";

        Optional<Share> share = shareRepo.findShareByToken(X_TOKEN);
        share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
        Share shareT = share.get();

        // 10분 이내
        LocalDateTime nowTime = LocalDateTime.now();
        LocalDateTime plusMinutes = shareT.getRegDate().plusMinutes(10);
        if(plusMinutes.isBefore(nowTime)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_001);

    }
    @Test
    public void 제약사항_같은방(){
        String X_TOKEN = "cbt";
        Long X_ROOM = 10l;

        Optional<Share> share = shareRepo.findShareByToken(X_TOKEN);
        share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
        Share shareT = share.get();
        // 동일한 방 체크
        if(!shareT.getRoomId().equals(X_ROOM)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_002);
    }
    @Test
    public void 제약사항_방장이요청(){
        String X_TOKEN = "cbt";
        Long X_USER = 1l;

        Optional<Share> share = shareRepo.findShareByToken(X_TOKEN);
        share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
        Share shareT = share.get();
        // 뿌리기한 사람이 요청시
        if(shareT.getMasterId().equals(X_USER)) throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_003);
    }
    @Test
    public void 제약사항_한번만가능(){
        String X_TOKEN = "cbt";
        Long X_USER = 1l;

        Optional<Share> share = shareRepo.findShareByToken(X_TOKEN);
        share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
        Share shareT = share.get();

        // 뿌리기 당 한번만 요청 가능
        Optional<ShareItem> shareItemT = Optional.empty();
        List<ShareItem> shareItems = shareT.getShareItems();
        for(ShareItem shareItem:shareItems){
            if(shareItem.getUserId() == X_USER){
                throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_004);
            }else if(shareItem.getReceiveYn().equals(false)){
                shareItemT = Optional.of(shareItem);
            }
        }
    }
    @Test
    public void 제약사항_선착순마감(){
        String X_TOKEN = "cbt";
        Long X_USER = 1l;

        Optional<Share> share = shareRepo.findShareByToken(X_TOKEN);
        share.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_000));
        Share shareT = share.get();

        // 뿌리기 당 한번만 요청 가능
        Optional<ShareItem> shareItemT = Optional.empty();
        List<ShareItem> shareItems = shareT.getShareItems();
        for(ShareItem shareItem:shareItems){
            if(shareItem.getUserId() == X_USER){
                throw new MoneyShareException(HttpStatus.BAD_REQUEST,ResponseMsg.RESPONSE_MSG_BAD_004);
            }else if(shareItem.getReceiveYn().equals(false)){
                shareItemT = Optional.of(shareItem);
            }
        }
        // 선착순 마감
        shareItemT.orElseThrow(()-> new MoneyShareException(HttpStatus.BAD_REQUEST, ResponseMsg.RESPONSE_MSG_BAD_005));
    }
}
